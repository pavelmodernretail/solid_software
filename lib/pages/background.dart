import 'dart:math';

import 'package:flutter/material.dart';
import 'package:solid_software/pages/persons.dart';
import 'persons.dart';

class BacgroundRandom extends StatefulWidget {
  @override
  _BacgroundRandomState createState() => _BacgroundRandomState();
}

class _BacgroundRandomState extends State<BacgroundRandom> {
  Color _color;

  @override
  void initState() {
    super.initState();
    _color = Colors.white;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: _color,
      appBar: AppBar(
        title: Text('Solid Software'),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.person_add),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => PersonPage())
              );
            },
          )
        ],
      ),
      body: Center(
        child: Container(
          height: 50.0,
          width: 100.0,
          padding: EdgeInsets.all(10.0),
          decoration: BoxDecoration(
            color: Colors.lightBlueAccent,
            borderRadius: BorderRadius.circular(40.0),
          ),
          child: InkWell(
            onTap: () {
              setState(() {
                _color = ColorGenerator.getColor();
              });
            },
            child: Center(
              child: Text('Click me'),
            ),
          ),
        ),
      ),
    );
  }
}

class ColorGenerator {
  static Random random = new Random();
  static Color getColor() {
    return Color.fromARGB(
        255, random.nextInt(255), random.nextInt(255), random.nextInt(255));
  }
}
